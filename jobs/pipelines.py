from scrapy.pipelines.media import MediaPipeline
from scrapy import Request
import pymongo

class JobsPipeline(object):
    def process_item(self, item, spider):
        #print(f"process_item: {item}")
        return item


def clean(text):
    text = text.strip()
    text = text.replace('\xad', '')
    return text


class IFramePipeline(MediaPipeline):

    def get_media_requests(self, item, info):
        """Returns the media requests to download"""
        return [Request(item.get('iframe_url'))]

    def media_downloaded(self, response, request, info):
        """Handler for success downloads"""
        text = ' '.join([s for s in map(clean, response.xpath('//body//*[not(self::script)]/text()').getall()) if len(s) > 0])
        return text

    def item_completed(self, results, item, info):
        """Called per item when all media requests has been processed"""
        print(f"results: {results}")
        (ok, text) = results[0]
        item['text'] = text
        return item


class MongoPipeline():
    """Ablage der gecrawlten Items in einer MongoDB"""
    
    collection_name = 'items'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        job = dict(item)
        job['_id'] = job['job_id']
        
        self.db[self.collection_name].find_one_and_replace({'_id': { '$eq' : job['job_id'] } }, job, upsert=True)
        return item