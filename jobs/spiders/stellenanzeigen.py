import json
import scrapy
from scrapy.linkextractors import LinkExtractor
from ..items import JobItem


def clean(text):
    return ' '.join([s for s in map(str.strip, text) if len(s) > 0])

class JobSpider(scrapy.Spider):
    """Crawler für die Seite stellenanzeigen.de"""
    
    name = "stellenanzeigen.de"
    
    custom_settings = {
        'DOWNLOAD_FAIL_ON_DATALOSS': True,
        'DOWNLOAD_DELAY': 0.5,
    }
    
    link_extractor = LinkExtractor(restrict_css=('a.position-link'))
    iframe_extractor = LinkExtractor(tags=('iframe'), attrs=('src'), restrict_css=('iframe.jobview-iframe'))

    def start_requests(self):
        urls = [
            "https://www.stellenanzeigen.de/stellenangebote/it/",
            "https://www.stellenanzeigen.de/stellenangebote/ingenieur/",
            "https://www.stellenanzeigen.de/stellenangebote/medizin/",
            "https://www.stellenanzeigen.de/stellenangebote/",
            #"https://www.stellenanzeigen.de/job/tourismuskaufmann-reiseverkehrskaufmann-kaufmaennischer-mitarbeiter-als-projektmanager-m-w-d-2938705/"
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, cb_kwargs={ 'form': True})

    def parse(self, response, form=False):
        page = response.url.split("/")
        links = self.link_extractor.extract_links(response)
        iframes = self.iframe_extractor.extract_links(response)
        
        for job in response.css('div.jobview-header'):
            #print('job found')
            item = JobItem()
            item['job_id'] = response.css('.position-active').xpath('@data-autowert').get()
            item['job_url'] = response.xpath('//link[@rel="canonical"]/@href').get()
            item['title'] = clean(job.css('.jobview-header-title').xpath('.//text()').getall())
            item['company'] = job.css('.jobview-header-company ::text').get()
            item['location'] = job.css('.jobview-header-region ::text').get()
            item['date'] = job.css('.jobview-header-date ::text').get()
            item['iframe_url'] = iframes[0].url
            #print(item)
            yield item

        if form:
            for div in response.css('div.joblist'):
                print(f"div: {div}")
                jsonSuchparameter = div.xpath('@data-jsonsuchparameter').get()
                url = 'https://www.stellenanzeigen.de/general/anzeigeteaserendlos/'
                form = { 'JsonSuchparameter' : jsonSuchparameter, 'Seite': '2'}
                request = scrapy.FormRequest(url, formdata=form, callback=self.parse_next, cb_kwargs={ 'seite': 2 }, priority=400)
                yield request

        for link in links:
            pass
            #print(f"link: {link}")
            yield scrapy.Request(link.url)

    def parse_next(self, response, seite):
        print(f"response: {response}")
        print(f"seite: {seite}")
        data = json.loads(response.text)
        print(f"data: {data}")
        
        data_response = scrapy.http.TextResponse(response.url, encoding='utf-8', body=data['data'])
        links = self.link_extractor.extract_links(data_response)
        
        if len(links) > 0:
            url = 'https://www.stellenanzeigen.de/general/anzeigeteaserendlos/'
            form = { 'JsonSuchparameter' : data['jsonsuchparameter'], 'Seite': str(seite + 1)}
            request = scrapy.FormRequest(url, formdata=form, callback=self.parse_next, cb_kwargs={ 'seite': seite + 1 }, priority=400)
            yield request
        
        for link in links:
            pass
            print(f"link: {link}")
            yield scrapy.Request(link.url, callback=self.parse)



            
