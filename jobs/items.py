# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class JobItem(scrapy.Item):
    """Klasse zur Speicherung der Stellenangebote"""
    job_id = scrapy.Field()
    job_url = scrapy.Field()
    title = scrapy.Field()
    company = scrapy.Field()
    location = scrapy.Field()
    text = scrapy.Field()
    date = scrapy.Field()
    link = scrapy.Field()
    iframe_url = scrapy.Field()
    files = scrapy.Field()
